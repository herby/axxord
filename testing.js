define([
    './deploy',
    'amber_core/SUnit',
    // --- packages used only during automated testing begin here ---
    'axxord/Axxord-Tests'
    // --- packages used only during automated testing end here ---
], function (amber) {
    return amber;
});
