define([
    'amber/deploy',
    // --- packages to be deployed begin here ---
    "axxord/Axxord"
    // --- packages to be deployed end here ---
], function (amber) {
    return amber;
});
