define(["amber/boot"
//>>excludeStart("imports", pragmas.excludeImports);
, "axxord/Axxord-Axon"
//>>excludeEnd("imports");
, "amber_core/Kernel-Collections", "amber_core/Kernel-Infrastructure", "amber_core/Kernel-Objects"], function($boot
//>>excludeStart("imports", pragmas.excludeImports);

//>>excludeEnd("imports");
){"use strict";
if(!("nilAsValue" in $boot))$boot.nilAsValue=$boot.nilAsReceiver;
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
$core.addPackage("Axxord");
($core.packageDescriptors||$core.packages)["Axxord"].innerEval = function (expr) { return eval(expr); };
($core.packageDescriptors||$core.packages)["Axxord"].imports = ["axxord/Axxord-Axon"];
($core.packageDescriptors||$core.packages)["Axxord"].transport = {"type":"amd","amdNamespace":"axxord"};

$core.addClass("Axes", $globals.Object, [], "Axxord");

$core.addMethod(
$core.method({
selector: "newInterestThru:doing:",
protocol: "factory",
fn: function (anAspect,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $2,$3,$1,$5,$4;
return $recv($recv($globals.PluggableInterest)._new())._accept_enact_((function(aspect){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=$recv(aspect)._size();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["size"]=1;
//>>excludeEnd("ctx");
$3=$recv(anAspect)._size();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["size"]=2;
//>>excludeEnd("ctx");
$1=$recv($2).__lt_eq($3);
if($core.assert($1)){
$5=$recv(aspect)._size();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["size"]=3;
//>>excludeEnd("ctx");
$4=$recv(anAspect)._copyFrom_to_((1),$5);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["copyFrom:to:"]=1;
//>>excludeEnd("ctx");
return $recv(aspect).__eq($4);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["="]=1;
//>>excludeEnd("ctx");
} else {
return $recv(anAspect).__eq($recv(aspect)._copyFrom_to_((1),$recv(anAspect)._size()));
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({aspect:aspect},$ctx1,1)});
//>>excludeEnd("ctx");
}),aBlock);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"newInterestThru:doing:",{anAspect:anAspect,aBlock:aBlock},$globals.Axes.a$cls)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAspect", "aBlock"],
source: "newInterestThru: anAspect doing: aBlock\x0a\x09^ PluggableInterest new\x0a\x09\x09accept: [ :aspect | aspect size <= anAspect size\x0a\x09\x09\x09ifTrue: [ aspect = (anAspect copyFrom: 1 to: aspect size) ]\x0a\x09\x09\x09ifFalse: [ anAspect = (aspect copyFrom: 1 to: anAspect size) ] ]\x0a\x09\x09enact: aBlock",
referencedClasses: ["PluggableInterest"],
//>>excludeEnd("ide");
messageSends: ["accept:enact:", "new", "ifTrue:ifFalse:", "<=", "size", "=", "copyFrom:to:"]
}),
$globals.Axes.a$cls);

$core.addMethod(
$core.method({
selector: "newInterestUpTo:doing:",
protocol: "factory",
fn: function (anAspect,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $2,$3,$1;
return $recv($recv($globals.PluggableInterest)._new())._accept_enact_((function(changedAspect){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=$recv(changedAspect)._size();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["size"]=1;
//>>excludeEnd("ctx");
$3=$recv(anAspect)._size();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["size"]=2;
//>>excludeEnd("ctx");
$1=$recv($2).__lt_eq($3);
return $recv($1)._and_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv(changedAspect).__eq($recv(anAspect)._copyFrom_to_((1),$recv(changedAspect)._size()));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({changedAspect:changedAspect},$ctx1,1)});
//>>excludeEnd("ctx");
}),aBlock);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"newInterestUpTo:doing:",{anAspect:anAspect,aBlock:aBlock},$globals.Axes.a$cls)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAspect", "aBlock"],
source: "newInterestUpTo: anAspect doing: aBlock\x0a\x09^ PluggableInterest new\x0a\x09\x09accept: [ :changedAspect | changedAspect size <= anAspect size and:\x0a\x09\x09\x09[changedAspect = (anAspect copyFrom: 1 to: changedAspect size)] ]\x0a\x09\x09enact: aBlock",
referencedClasses: ["PluggableInterest"],
//>>excludeEnd("ide");
messageSends: ["accept:enact:", "new", "and:", "<=", "size", "=", "copyFrom:to:"]
}),
$globals.Axes.a$cls);

$core.addMethod(
$core.method({
selector: "on:at:consume:",
protocol: "delegated",
fn: function (anObject,aCollection,aBlock){
var self=this,$self=this;
var value;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $early={};
try {
value=$recv(anObject)._atAxes_ifAbsent_(aCollection,(function(){
throw $early=[anObject];

}));
return $recv(aBlock)._value_(value);
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:at:consume:",{anObject:anObject,aCollection:aCollection,aBlock:aBlock,value:value},$globals.Axes.a$cls)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aCollection", "aBlock"],
source: "on: anObject at: aCollection consume: aBlock\x0a\x09| value |\x0a\x09value := anObject atAxes: aCollection ifAbsent: [ ^ anObject ].\x0a\x09^ aBlock value: value",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["atAxes:ifAbsent:", "value:"]
}),
$globals.Axes.a$cls);

$core.addMethod(
$core.method({
selector: "on:at:ifAbsent:",
protocol: "delegated",
fn: function (anObject,aCollection,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $early={};
try {
return $recv(aCollection)._inject_into_(anObject,(function(soFar,segment){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(segment)._asAxisIn_ifAbsent_(soFar,(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
throw $early=[$recv(aBlock)._value()];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({soFar:soFar,segment:segment},$ctx1,1)});
//>>excludeEnd("ctx");
}));
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:at:ifAbsent:",{anObject:anObject,aCollection:aCollection,aBlock:aBlock},$globals.Axes.a$cls)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aCollection", "aBlock"],
source: "on: anObject at: aCollection ifAbsent: aBlock\x0a\x09^ aCollection inject: anObject into: [ :soFar :segment |\x0a\x09\x09segment asAxisIn: soFar ifAbsent: [ ^ aBlock value ]]",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["inject:into:", "asAxisIn:ifAbsent:", "value"]
}),
$globals.Axes.a$cls);

$core.addMethod(
$core.method({
selector: "on:at:ifAbsent:put:",
protocol: "delegated",
fn: function (anObject,aCollection,aBlock,value){
var self=this,$self=this;
var penultimate;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
penultimate=$recv(anObject)._atAxes_ifAbsent_($recv(aCollection)._allButLast(),aBlock);
return $recv($recv(aCollection)._last())._asAxisIn_ifAbsent_put_(penultimate,aBlock,value);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:at:ifAbsent:put:",{anObject:anObject,aCollection:aCollection,aBlock:aBlock,value:value,penultimate:penultimate},$globals.Axes.a$cls)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aCollection", "aBlock", "value"],
source: "on: anObject at: aCollection ifAbsent: aBlock put: value\x0a\x09| penultimate |\x0a\x09penultimate := anObject atAxes: aCollection allButLast ifAbsent: aBlock.\x0a\x09^ aCollection last asAxisIn: penultimate ifAbsent: aBlock put: value",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["atAxes:ifAbsent:", "allButLast", "asAxisIn:ifAbsent:put:", "last"]
}),
$globals.Axes.a$cls);

$core.addMethod(
$core.method({
selector: "on:at:transform:",
protocol: "delegated",
fn: function (anObject,aCollection,aBlock){
var self=this,$self=this;
var value,newValue;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$receiver;
var $early={};
try {
value=$recv(anObject)._atAxes_ifAbsent_(aCollection,(function(){
throw $early=[anObject];

}));
newValue=$recv(aBlock)._value_(value);
$1=$recv(value).__eq_eq(newValue);
if(!$core.assert($1)){
$recv(anObject)._atAxes_ifAbsent_put_(aCollection,(function(){
throw $early=[anObject];

}),newValue);
}
$2=$recv(anObject)._axxord();
if(($receiver = $2) == null || $receiver.a$nil){
$2;
} else {
var axon;
axon=$receiver;
$recv(axon)._changed_(aCollection);
}
return self;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:at:transform:",{anObject:anObject,aCollection:aCollection,aBlock:aBlock,value:value,newValue:newValue},$globals.Axes.a$cls)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aCollection", "aBlock"],
source: "on: anObject at: aCollection transform: aBlock\x0a\x09| value newValue |\x0a\x09value := anObject atAxes: aCollection ifAbsent: [ ^ anObject ].\x0a\x09newValue := aBlock value: value.\x0a\x09value == newValue ifFalse: [ anObject atAxes: aCollection ifAbsent: [ ^ anObject ] put: newValue ].\x0a\x09anObject axxord ifNotNil: [:axon | axon changed: aCollection]",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["atAxes:ifAbsent:", "value:", "ifFalse:", "==", "atAxes:ifAbsent:put:", "ifNotNil:", "axxord", "changed:"]
}),
$globals.Axes.a$cls);

$core.addMethod(
$core.method({
selector: "parse:",
protocol: "parsing",
fn: function (message){
var self=this,$self=this;
var result,stack,anArray;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1,$2,$3,$5,$4,$6,$7,$8,$9,$10,$11;
anArray=$recv(message)._tokenize_(" ");
result=[];
stack=[result];
$recv(anArray)._do_((function(each){
var asNum,inner,close;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
close=(0);
close;
inner=each;
inner;
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
$1=$recv(inner)._notEmpty();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx3.sendIdx["notEmpty"]=1;
//>>excludeEnd("ctx");
return $recv($1)._and_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx4) {
//>>excludeEnd("ctx");
$2=$recv(inner)._first();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx4.sendIdx["first"]=1;
//>>excludeEnd("ctx");
return $recv($2).__eq("(");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx4.sendIdx["="]=1;
//>>excludeEnd("ctx");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx4) {$ctx4.fillBlock({},$ctx3,3)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx3.sendIdx["and:"]=1;
//>>excludeEnd("ctx");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,2)});
//>>excludeEnd("ctx");
}))._whileTrue_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
inner=$recv(inner)._allButFirst();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx3.sendIdx["allButFirst"]=1;
//>>excludeEnd("ctx");
inner;
$3=stack;
$5=$recv(stack)._last();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx3.sendIdx["last"]=1;
//>>excludeEnd("ctx");
$4=$recv($5)._add_([]);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx3.sendIdx["add:"]=2;
//>>excludeEnd("ctx");
return $recv($3)._add_($4);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx3.sendIdx["add:"]=1;
//>>excludeEnd("ctx");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,4)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["whileTrue:"]=1;
//>>excludeEnd("ctx");
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
$6=$recv(inner)._notEmpty();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx3.sendIdx["notEmpty"]=2;
//>>excludeEnd("ctx");
return $recv($6)._and_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx4) {
//>>excludeEnd("ctx");
$7=$recv(inner)._last();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx4.sendIdx["last"]=2;
//>>excludeEnd("ctx");
return $recv($7).__eq(")");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx4.sendIdx["="]=2;
//>>excludeEnd("ctx");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx4) {$ctx4.fillBlock({},$ctx3,6)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx3.sendIdx["and:"]=2;
//>>excludeEnd("ctx");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,5)});
//>>excludeEnd("ctx");
}))._whileTrue_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
inner=$recv(inner)._allButLast();
inner;
close=$recv(close).__plus((1));
return close;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,7)});
//>>excludeEnd("ctx");
}));
$8=$recv($recv(inner)._notEmpty())._and_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($recv(inner)._first()).__eq("~");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx3.sendIdx["="]=3;
//>>excludeEnd("ctx");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,8)});
//>>excludeEnd("ctx");
}));
if($core.assert($8)){
inner=[$recv(inner)._allButFirst()];
inner;
}
$9=$recv(inner)._isString();
if($core.assert($9)){
asNum=$recv($recv(inner)._ifEmpty_((function(){
return "NaN";

})))._asNumber();
} else {
asNum=inner;
}
asNum;
$10=$recv(asNum).__eq(asNum);
if($core.assert($10)){
$11=$recv(stack)._last();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["last"]=3;
//>>excludeEnd("ctx");
$recv($11)._add_(asNum);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["add:"]=3;
//>>excludeEnd("ctx");
} else {
$recv(inner)._ifNotEmpty_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($recv(stack)._last())._add_(inner);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,15)});
//>>excludeEnd("ctx");
}));
}
return $recv(close)._timesRepeat_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv(stack)._removeLast();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,16)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({each:each,asNum:asNum,inner:inner,close:close},$ctx1,1)});
//>>excludeEnd("ctx");
}));
return result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"parse:",{message:message,result:result,stack:stack,anArray:anArray},$globals.Axes.a$cls)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["message"],
source: "parse: message\x0a\x09| result stack anArray |\x0a\x09anArray := message tokenize: ' '.\x0a\x09result := #().\x0a\x09stack := { result }.\x0a\x09anArray do: [ :each |\x0a\x09\x09| asNum inner close |\x0a\x09\x09close := 0.\x0a\x09\x09inner := each.\x0a\x09\x09[ inner notEmpty and: [ inner first = '(' ]] whileTrue: [ inner := inner allButFirst. stack add: (stack last add: #()) ].\x0a\x09\x09[ inner notEmpty and: [ inner last = ')' ]] whileTrue: [ inner := inner allButLast. close := close + 1 ].\x0a\x09\x09(inner notEmpty and: [ inner first = '~' ]) ifTrue: [ inner := { inner allButFirst } ].\x0a\x09\x09asNum := inner isString ifTrue: [ (inner ifEmpty: [ 'NaN' ]) asNumber ] ifFalse: [ inner ].\x0a\x09\x09asNum = asNum ifTrue: [ stack last add: asNum ] ifFalse: [\x0a\x09\x09\x09inner ifNotEmpty: [ stack last add: inner ] ].\x0a\x09\x09close timesRepeat: [ stack removeLast ] ].\x0a\x09^ result",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["tokenize:", "do:", "whileTrue:", "and:", "notEmpty", "=", "first", "allButFirst", "add:", "last", "allButLast", "+", "ifTrue:", "ifTrue:ifFalse:", "isString", "asNumber", "ifEmpty:", "ifNotEmpty:", "timesRepeat:", "removeLast"]
}),
$globals.Axes.a$cls);


$core.addClass("Axolator", $globals.Object, ["root"], "Axxord");
$core.addMethod(
$core.method({
selector: "atAxes:ifAbsent:",
protocol: "action",
fn: function (aCollection,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self["@root"])._atAxes_ifAbsent_(aCollection,aBlock);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"atAxes:ifAbsent:",{aCollection:aCollection,aBlock:aBlock},$globals.Axolator)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection", "aBlock"],
source: "atAxes: aCollection ifAbsent: aBlock\x0a\x09^ root atAxes: aCollection ifAbsent: aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["atAxes:ifAbsent:"]
}),
$globals.Axolator);

$core.addMethod(
$core.method({
selector: "atAxes:ifAbsent:put:",
protocol: "action",
fn: function (aCollection,aBlock,value){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self["@root"])._atAxes_ifAbsent_put_(aCollection,aBlock,value);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"atAxes:ifAbsent:put:",{aCollection:aCollection,aBlock:aBlock,value:value},$globals.Axolator)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection", "aBlock", "value"],
source: "atAxes: aCollection ifAbsent: aBlock put: value\x0a\x09^ root atAxes: aCollection ifAbsent: aBlock put: value",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["atAxes:ifAbsent:put:"]
}),
$globals.Axolator);

$core.addMethod(
$core.method({
selector: "axes:consume:",
protocol: "action",
fn: function (aCollection,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($globals.Axolator.superclass||$boot.nilAsClass).fn.prototype._axes_consume_.apply($self, [aCollection,(function(value){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(aBlock)._value_($recv(value)._deepCopy());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({value:value},$ctx1,1)});
//>>excludeEnd("ctx");
})]));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = false;
//>>excludeEnd("ctx");;
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"axes:consume:",{aCollection:aCollection,aBlock:aBlock},$globals.Axolator)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection", "aBlock"],
source: "axes: aCollection consume: aBlock\x0a\x0asuper axes: aCollection consume: [:value | aBlock value: value deepCopy]",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["axes:consume:", "value:", "deepCopy"]
}),
$globals.Axolator);

$core.addMethod(
$core.method({
selector: "axes:transform:",
protocol: "action",
fn: function (aCollection,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $2,$1,$3,$receiver;
$recv(aCollection)._ifEmpty_ifNotEmpty_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$2=$recv(aBlock)._value_($self._root());
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["value:"]=1;
//>>excludeEnd("ctx");
$1=$recv($2)._deepCopy();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["deepCopy"]=1;
//>>excludeEnd("ctx");
$self._root_($1);
$3=$self._axxord();
if(($receiver = $3) == null || $receiver.a$nil){
return $3;
} else {
var axxord;
axxord=$receiver;
return $recv(axxord)._changed_(aCollection);
}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}),(function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return (
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.supercall = true,
//>>excludeEnd("ctx");
($globals.Axolator.superclass||$boot.nilAsClass).fn.prototype._axes_transform_.apply($self, [aCollection,(function(value){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($recv(aBlock)._value_(value))._deepCopy();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({value:value},$ctx2,4)});
//>>excludeEnd("ctx");
})]));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.supercall = false;
//>>excludeEnd("ctx");;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,3)});
//>>excludeEnd("ctx");
}));
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"axes:transform:",{aCollection:aCollection,aBlock:aBlock},$globals.Axolator)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection", "aBlock"],
source: "axes: aCollection transform: aBlock\x0a\x0aaCollection\x0a\x09ifEmpty: [ self root: (aBlock value: self root) deepCopy. self axxord ifNotNil: [ :axxord | axxord changed: aCollection ] ]\x0a\x09ifNotEmpty: [ super axes: aCollection transform: [:value | (aBlock value: value) deepCopy] ]",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["ifEmpty:ifNotEmpty:", "root:", "deepCopy", "value:", "root", "ifNotNil:", "axxord", "changed:", "axes:transform:"]
}),
$globals.Axolator);

$core.addMethod(
$core.method({
selector: "root",
protocol: "accessing",
fn: function (){
var self=this,$self=this;
return $self["@root"];

},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "root\x0a\x0a^root",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: []
}),
$globals.Axolator);

$core.addMethod(
$core.method({
selector: "root:",
protocol: "accessing",
fn: function (anObject){
var self=this,$self=this;
$self["@root"]=anObject;
return self;

},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "root: anObject\x0a\x0aroot := anObject",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: []
}),
$globals.Axolator);


$core.addMethod(
$core.method({
selector: "on:",
protocol: "instance creation",
fn: function (anObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($self._new())._root_(anObject);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"on:",{anObject:anObject},$globals.Axolator.a$cls)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "on: anObject\x0a^self new root: anObject",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["root:", "new"]
}),
$globals.Axolator.a$cls);

$core.addMethod(
$core.method({
selector: "asAxisIn:ifAbsent:",
protocol: "*Axxord",
fn: function (anObject,aBlock){
var self=this,$self=this;
var receiver,selector,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $5,$4,$3,$2,$1;
var $early={};
try {
selector=$self._first();
receiver=$recv(anObject)._yourself();
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
result=$recv(receiver)._perform_(selector);
return result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._on_do_($globals.MessageNotUnderstood,(function(mnu){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$5=$recv(mnu)._message();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["message"]=1;
//>>excludeEnd("ctx");
$4=$recv($5)._selector();
$3=$recv($4).__eq(selector);
$2=$recv($3)._and_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($recv(mnu)._receiver()).__eq_eq(receiver);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,3)});
//>>excludeEnd("ctx");
}));
$1=$recv($2)._and_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($recv($recv(mnu)._message())._arguments())._isEmpty();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,4)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["and:"]=1;
//>>excludeEnd("ctx");
if(!$core.assert($1)){
$recv(mnu)._resignal();
}
throw $early=[$recv(aBlock)._value()];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({mnu:mnu},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return result;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"asAxisIn:ifAbsent:",{anObject:anObject,aBlock:aBlock,receiver:receiver,selector:selector,result:result},$globals.Array)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aBlock"],
source: "asAxisIn: anObject ifAbsent: aBlock\x0a\x09| receiver selector result |\x0a\x09selector := self first.\x0a\x09receiver := anObject yourself. \x22JSObjectProxy hack\x22\x0a\x09\x0a\x09[ result := receiver perform: selector ]\x0a\x09on: MessageNotUnderstood do: [ :mnu |\x0a\x09\x09((mnu message selector = selector\x0a\x09\x09\x09and: [ mnu receiver == receiver ])\x0a\x09\x09\x09and: [ mnu message arguments isEmpty ])\x0a\x09\x09\x09ifFalse: [ mnu resignal ].\x0a\x09\x09^ aBlock value ].\x0a\x09^ result",
referencedClasses: ["MessageNotUnderstood"],
//>>excludeEnd("ide");
messageSends: ["first", "yourself", "on:do:", "perform:", "ifFalse:", "and:", "=", "selector", "message", "==", "receiver", "isEmpty", "arguments", "resignal", "value"]
}),
$globals.Array);

$core.addMethod(
$core.method({
selector: "asAxisIn:ifAbsent:put:",
protocol: "*Axxord",
fn: function (anObject,aBlock,anotherObject){
var self=this,$self=this;
var receiver,selector,arguments_,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $5,$4,$3,$2,$1;
var $early={};
try {
selector=$recv($self._first())._asMutator();
receiver=$recv(anObject)._yourself();
arguments_=[anotherObject];
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
result=$recv(receiver)._perform_withArguments_(selector,arguments_);
return result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._on_do_($globals.MessageNotUnderstood,(function(mnu){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
$5=$recv(mnu)._message();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["message"]=1;
//>>excludeEnd("ctx");
$4=$recv($5)._selector();
$3=$recv($4).__eq(selector);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["="]=1;
//>>excludeEnd("ctx");
$2=$recv($3)._and_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($recv(mnu)._receiver()).__eq_eq(receiver);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,3)});
//>>excludeEnd("ctx");
}));
$1=$recv($2)._and_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx3) {
//>>excludeEnd("ctx");
return $recv($recv($recv(mnu)._message())._arguments()).__eq(arguments_);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx3) {$ctx3.fillBlock({},$ctx2,4)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["and:"]=1;
//>>excludeEnd("ctx");
if(!$core.assert($1)){
$recv(mnu)._resignal();
}
throw $early=[$recv(aBlock)._value()];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({mnu:mnu},$ctx1,2)});
//>>excludeEnd("ctx");
}));
return result;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"asAxisIn:ifAbsent:put:",{anObject:anObject,aBlock:aBlock,anotherObject:anotherObject,receiver:receiver,selector:selector,arguments_:arguments_,result:result},$globals.Array)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aBlock", "anotherObject"],
source: "asAxisIn: anObject ifAbsent: aBlock put: anotherObject\x0a\x09| receiver selector arguments result |\x0a\x09selector := self first asMutator.\x0a\x09receiver := anObject yourself. \x22JSObjectProxy hack\x22\x0a\x09arguments := { anotherObject }.\x0a\x09\x0a\x09[ result := receiver perform: selector withArguments: arguments ]\x0a\x09on: MessageNotUnderstood do: [ :mnu |\x0a\x09\x09((mnu message selector = selector\x0a\x09\x09\x09and: [ mnu receiver == receiver ])\x0a\x09\x09\x09and: [ mnu message arguments = arguments ])\x0a\x09\x09\x09ifFalse: [ mnu resignal ].\x0a\x09\x09^ aBlock value ].\x0a\x09^ result",
referencedClasses: ["MessageNotUnderstood"],
//>>excludeEnd("ide");
messageSends: ["asMutator", "first", "yourself", "on:do:", "perform:withArguments:", "ifFalse:", "and:", "=", "selector", "message", "==", "receiver", "arguments", "resignal", "value"]
}),
$globals.Array);

$core.addMethod(
$core.method({
selector: "asAxisIn:ifAbsent:",
protocol: "*Axxord",
fn: function (anObject,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv(aBlock)._value();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"asAxisIn:ifAbsent:",{anObject:anObject,aBlock:aBlock},$globals.JSObjectProxy)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aBlock"],
source: "asAxisIn: anObject ifAbsent: aBlock\x0a\x09^ aBlock value",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["value"]
}),
$globals.JSObjectProxy);

$core.addMethod(
$core.method({
selector: "asAxisIn:ifAbsent:put:",
protocol: "*Axxord",
fn: function (anObject,aBlock,anotherObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv(aBlock)._value();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"asAxisIn:ifAbsent:put:",{anObject:anObject,aBlock:aBlock,anotherObject:anotherObject},$globals.JSObjectProxy)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aBlock", "anotherObject"],
source: "asAxisIn: anObject ifAbsent: aBlock put: anotherObject\x0a\x09^ aBlock value",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["value"]
}),
$globals.JSObjectProxy);

$core.addMethod(
$core.method({
selector: "atAxes:ifAbsent:",
protocol: "*Axxord",
fn: function (aCollection,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.Axes)._on_at_ifAbsent_(self,aCollection,aBlock);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"atAxes:ifAbsent:",{aCollection:aCollection,aBlock:aBlock},$globals.JSObjectProxy)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection", "aBlock"],
source: "atAxes: aCollection ifAbsent: aBlock\x0a\x09^ Axes on: self at: aCollection ifAbsent: aBlock",
referencedClasses: ["Axes"],
//>>excludeEnd("ide");
messageSends: ["on:at:ifAbsent:"]
}),
$globals.JSObjectProxy);

$core.addMethod(
$core.method({
selector: "atAxes:ifAbsent:put:",
protocol: "*Axxord",
fn: function (aCollection,aBlock,value){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.Axes)._on_at_ifAbsent_put_(self,aCollection,aBlock,value);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"atAxes:ifAbsent:put:",{aCollection:aCollection,aBlock:aBlock,value:value},$globals.JSObjectProxy)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection", "aBlock", "value"],
source: "atAxes: aCollection ifAbsent: aBlock put: value\x0a\x09^ Axes on: self at: aCollection ifAbsent: aBlock put: value",
referencedClasses: ["Axes"],
//>>excludeEnd("ide");
messageSends: ["on:at:ifAbsent:put:"]
}),
$globals.JSObjectProxy);

$core.addMethod(
$core.method({
selector: "axes:consume:",
protocol: "*Axxord",
fn: function (aCollection,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.Axes)._on_at_consume_(self,aCollection,aBlock);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"axes:consume:",{aCollection:aCollection,aBlock:aBlock},$globals.JSObjectProxy)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection", "aBlock"],
source: "axes: aCollection consume: aBlock\x0a\x09^ Axes on: self at: aCollection consume: aBlock",
referencedClasses: ["Axes"],
//>>excludeEnd("ide");
messageSends: ["on:at:consume:"]
}),
$globals.JSObjectProxy);

$core.addMethod(
$core.method({
selector: "axes:transform:",
protocol: "*Axxord",
fn: function (aCollection,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.Axes)._on_at_transform_(self,aCollection,aBlock);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"axes:transform:",{aCollection:aCollection,aBlock:aBlock},$globals.JSObjectProxy)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection", "aBlock"],
source: "axes: aCollection transform: aBlock\x0a\x09^ Axes on: self at: aCollection transform: aBlock",
referencedClasses: ["Axes"],
//>>excludeEnd("ide");
messageSends: ["on:at:transform:"]
}),
$globals.JSObjectProxy);

$core.addMethod(
$core.method({
selector: "axxord",
protocol: "*Axxord",
fn: function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $self["@jsObject"].$axxord$;
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"axxord",{},$globals.JSObjectProxy)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "axxord\x0a<inlineJS: 'return $self[\x22@jsObject\x22].$axxord$'>",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: []
}),
$globals.JSObjectProxy);

$core.addMethod(
$core.method({
selector: "axxord:",
protocol: "*Axxord",
fn: function (anAxon){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$self["@jsObject"].$axxord$ = anAxon;
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"axxord:",{anAxon:anAxon},$globals.JSObjectProxy)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAxon"],
source: "axxord: anAxon\x0a<inlineJS: '$self[\x22@jsObject\x22].$axxord$ = anAxon'>",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: []
}),
$globals.JSObjectProxy);

$core.addMethod(
$core.method({
selector: "asAxisIn:ifAbsent:",
protocol: "*Axxord",
fn: function (anObject,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
var $early={};
try {
$1=$recv(anObject)._respondsTo_("at:ifAbsent:");
$recv($1)._ifTrue_ifFalse_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
throw $early=[$recv(anObject)._at_ifAbsent_(self,aBlock)];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}),aBlock);
return self;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"asAxisIn:ifAbsent:",{anObject:anObject,aBlock:aBlock},$globals.Number)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aBlock"],
source: "asAxisIn: anObject ifAbsent: aBlock\x0a\x09(anObject respondsTo: #at:ifAbsent:)\x0a\x09\x09ifTrue: [ ^ anObject at: self ifAbsent: aBlock ]\x0a\x09\x09ifFalse: aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["ifTrue:ifFalse:", "respondsTo:", "at:ifAbsent:"]
}),
$globals.Number);

$core.addMethod(
$core.method({
selector: "asAxisIn:ifAbsent:put:",
protocol: "*Axxord",
fn: function (anObject,aBlock,anotherObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
var $early={};
try {
$1=$recv(anObject)._respondsTo_("at:put:");
$recv($1)._ifTrue_ifFalse_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
throw $early=[$recv(anObject)._at_put_(self,anotherObject)];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}),aBlock);
return self;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"asAxisIn:ifAbsent:put:",{anObject:anObject,aBlock:aBlock,anotherObject:anotherObject},$globals.Number)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aBlock", "anotherObject"],
source: "asAxisIn: anObject ifAbsent: aBlock put: anotherObject\x0a\x09(anObject respondsTo: #at:put:)\x0a\x09\x09ifTrue: [ ^ anObject at: self put: anotherObject ]\x0a\x09\x09ifFalse: aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["ifTrue:ifFalse:", "respondsTo:", "at:put:"]
}),
$globals.Number);

$core.addMethod(
$core.method({
selector: "asAxisIn:ifAbsent:",
protocol: "*Axxord",
fn: function (anObject,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv(aBlock)._value();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"asAxisIn:ifAbsent:",{anObject:anObject,aBlock:aBlock},$globals.Object)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aBlock"],
source: "asAxisIn: anObject ifAbsent: aBlock\x0a\x09^ aBlock value",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["value"]
}),
$globals.Object);

$core.addMethod(
$core.method({
selector: "asAxisIn:ifAbsent:put:",
protocol: "*Axxord",
fn: function (anObject,aBlock,anotherObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv(aBlock)._value();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"asAxisIn:ifAbsent:put:",{anObject:anObject,aBlock:aBlock,anotherObject:anotherObject},$globals.Object)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aBlock", "anotherObject"],
source: "asAxisIn: anObject ifAbsent: aBlock put: anotherObject\x0a\x09^ aBlock value",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["value"]
}),
$globals.Object);

$core.addMethod(
$core.method({
selector: "atAxes:ifAbsent:",
protocol: "*Axxord",
fn: function (aCollection,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.Axes)._on_at_ifAbsent_(self,aCollection,aBlock);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"atAxes:ifAbsent:",{aCollection:aCollection,aBlock:aBlock},$globals.Object)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection", "aBlock"],
source: "atAxes: aCollection ifAbsent: aBlock\x0a\x09^ Axes on: self at: aCollection ifAbsent: aBlock",
referencedClasses: ["Axes"],
//>>excludeEnd("ide");
messageSends: ["on:at:ifAbsent:"]
}),
$globals.Object);

$core.addMethod(
$core.method({
selector: "atAxes:ifAbsent:put:",
protocol: "*Axxord",
fn: function (aCollection,aBlock,value){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.Axes)._on_at_ifAbsent_put_(self,aCollection,aBlock,value);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"atAxes:ifAbsent:put:",{aCollection:aCollection,aBlock:aBlock,value:value},$globals.Object)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection", "aBlock", "value"],
source: "atAxes: aCollection ifAbsent: aBlock put: value\x0a\x09^ Axes on: self at: aCollection ifAbsent: aBlock put: value",
referencedClasses: ["Axes"],
//>>excludeEnd("ide");
messageSends: ["on:at:ifAbsent:put:"]
}),
$globals.Object);

$core.addMethod(
$core.method({
selector: "axes:consume:",
protocol: "*Axxord",
fn: function (aCollection,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.Axes)._on_at_consume_(self,aCollection,aBlock);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"axes:consume:",{aCollection:aCollection,aBlock:aBlock},$globals.Object)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection", "aBlock"],
source: "axes: aCollection consume: aBlock\x0a\x09^ Axes on: self at: aCollection consume: aBlock",
referencedClasses: ["Axes"],
//>>excludeEnd("ide");
messageSends: ["on:at:consume:"]
}),
$globals.Object);

$core.addMethod(
$core.method({
selector: "axes:transform:",
protocol: "*Axxord",
fn: function (aCollection,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return $recv($globals.Axes)._on_at_transform_(self,aCollection,aBlock);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"axes:transform:",{aCollection:aCollection,aBlock:aBlock},$globals.Object)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["aCollection", "aBlock"],
source: "axes: aCollection transform: aBlock\x0a\x09^ Axes on: self at: aCollection transform: aBlock",
referencedClasses: ["Axes"],
//>>excludeEnd("ide");
messageSends: ["on:at:transform:"]
}),
$globals.Object);

$core.addMethod(
$core.method({
selector: "axxord",
protocol: "*Axxord",
fn: function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
return self.$axxord$;
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"axxord",{},$globals.Object)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "axxord\x0a<inlineJS: 'return self.$axxord$'>",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: []
}),
$globals.Object);

$core.addMethod(
$core.method({
selector: "axxord:",
protocol: "*Axxord",
fn: function (anAxon){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
self.$axxord$ = anAxon;
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"axxord:",{anAxon:anAxon},$globals.Object)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAxon"],
source: "axxord: anAxon\x0a<inlineJS: 'self.$axxord$ = anAxon'>",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: []
}),
$globals.Object);

$core.addMethod(
$core.method({
selector: "asAxisIn:ifAbsent:",
protocol: "*Axxord",
fn: function (anObject,aBlock){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
var $early={};
try {
$1=$recv(anObject)._respondsTo_("at:ifAbsent:");
$recv($1)._ifTrue_ifFalse_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
throw $early=[$recv(anObject)._at_ifAbsent_(self,aBlock)];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}),aBlock);
return self;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"asAxisIn:ifAbsent:",{anObject:anObject,aBlock:aBlock},$globals.String)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aBlock"],
source: "asAxisIn: anObject ifAbsent: aBlock\x0a\x09(anObject respondsTo: #at:ifAbsent:)\x0a\x09\x09ifTrue: [ ^ anObject at: self ifAbsent: aBlock ]\x0a\x09\x09ifFalse: aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["ifTrue:ifFalse:", "respondsTo:", "at:ifAbsent:"]
}),
$globals.String);

$core.addMethod(
$core.method({
selector: "asAxisIn:ifAbsent:put:",
protocol: "*Axxord",
fn: function (anObject,aBlock,anotherObject){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
var $early={};
try {
$1=$recv(anObject)._respondsTo_("at:put:");
$recv($1)._ifTrue_ifFalse_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
throw $early=[$recv(anObject)._at_put_(self,anotherObject)];
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}),aBlock);
return self;
}
catch(e) {if(e===$early)return e[0]; throw e}
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"asAxisIn:ifAbsent:put:",{anObject:anObject,aBlock:aBlock,anotherObject:anotherObject},$globals.String)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject", "aBlock", "anotherObject"],
source: "asAxisIn: anObject ifAbsent: aBlock put: anotherObject\x0a\x09(anObject respondsTo: #at:put:)\x0a\x09\x09ifTrue: [ ^ anObject at: self put: anotherObject ]\x0a\x09\x09ifFalse: aBlock",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["ifTrue:ifFalse:", "respondsTo:", "at:put:"]
}),
$globals.String);

});
