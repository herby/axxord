Smalltalk createPackage: 'Axxord-Tests'!
TestCase subclass: #AxolatorTest
	instanceVariableNames: 'rootModel'
	package: 'Axxord-Tests'!

!AxolatorTest methodsFor: 'tests'!

testNontrivialModelGetsAppropriateValueForModification
| isolator result |
result := nil.
isolator := Axolator on: #{ 'foo' -> #('bar' #(1 #(2 5)) 'baz'). 'moo' -> 'zoo' }.
isolator axes: #(foo 2) transform: [:r|result := r].
self assert: #(1 #(2 5)) equals: result
!

testNontrivialModelModifiesAppropriateValue
| isolator result |
result := nil.
isolator := Axolator on: #{ 'foo' -> #('bar' #(1 #(2 3)) 'baz'). 'moo' -> 'zoo' }.
isolator axes: #(foo 2) transform: [:r|#new].
isolator axes: #(foo 2) consume: [:r|result := r].
self assert: #new equals: result.
isolator axes: #() consume: [:r|result := r].
self assert: #{ 'foo' -> #('bar' #new 'baz'). 'moo' -> 'zoo' } equals: result
!

testNontrivialModelReturnsAppropriateValue
| isolator result |
result := nil.
isolator := Axolator on: #{ 'foo' -> #('bar' #(1 #(2 3)) 'baz'). 'moo' -> 'zoo' }.
isolator axes: #(foo 2) consume: [:r|result := r].
self assert: #(1 #(2 3)) equals: result
!

testRootModelExaminesThenModifiesRoot
| isolator result |
result := nil.
isolator := Axolator on: #(1 #(2 3)).
isolator axes: #() transform: [:r|r second].
isolator axes: #() consume: [:r|result := r].
self assert: #(2 3) equals: result
!

testRootModelGetsRootForModification
| isolator result |
result := nil.
isolator := Axolator on: #(2 #(1 0)).
isolator axes: #() transform: [:r|result := r].
self assert: #(2 #(1 0)) equals: result
!

testRootModelModifiesAndDeeplyIsolatesInPlaceModifiedRoot
| isolator result newValue |
result := nil. newValue := nil.
isolator := Axolator on: #(1 #(2 3)).
isolator axes: #() transform: [:r|newValue := r. r at: 1 put: 4. r].
newValue at: 2 put: 'bar'.
isolator axes: #() consume: [:r|result := r].
newValue at: 2 put: 'baz'.
self assert: #(4 #(2 3)) equals: result
!

testRootModelModifiesAndDeeplyIsolatesRoot
| isolator result newValue |
result := nil.
isolator := Axolator on: #(1 #(2 3)).
newValue := #{'foo'->#(4 5 6)}.
isolator axes: #() transform: [:r|newValue].
(newValue at: 'foo') at: 1 put: 'bar'.
isolator axes: #() consume: [:r|result := r].
(newValue at: 'foo') at: 3 put: 'baz'.
self assert: #{'foo'->#(4 5 6)} equals: result
!

testRootModelModifiesAndIsolatesRoot
| isolator result newValue |
result := nil.
isolator := Axolator on: #(1 #(2 3)).
newValue := #{'foo'->#(4 5 6)}.
isolator axes: #() transform: [:r|newValue].
newValue at: 'foo' put: 'bar'.
isolator axes: #() consume: [:r|result := r].
newValue at: 'foo' put: 'baz'.
self assert: #{'foo'->#(4 5 6)} equals: result
!

testRootModelModifiesRoot
| isolator result |
result := nil.
isolator := Axolator on: #(1 #(2 3)).
isolator axes: #() transform: [:r|#{'foo'->#(4 5 6)}].
isolator axes: #() consume: [:r|result := r].
self assert: #{'foo'->#(4 5 6)} equals: result
!

testRootModelReturnsDeeplyIsolatedRoot
| isolator result |
result := nil.
isolator := Axolator on: #(1 #(2 3)).
isolator axes: #() consume: [:r|(r at: 2) at: 1 put: 0].
isolator axes: #() consume: [:r|result := r].
self assert: #(1 #(2 3)) equals: result
!

testRootModelReturnsIsolatedRoot
| isolator result |
result := nil.
isolator := Axolator on: #(1 #(2 4)).
isolator axes: #() consume: [:r|r at: 2 put: nil].
isolator axes: #() consume: [:r|result := r].
self assert: #(1 #(2 4)) equals: result
!

testRootModelReturnsRoot
| isolator result |
result := nil.
isolator := Axolator on: #(1 #(2 3)).
isolator axes: #() consume: [:r|result := r].
self assert: #(1 #(2 3)) equals: result
! !

TestCase subclass: #PlainConsumeTransformTest
	instanceVariableNames: ''
	package: 'Axxord-Tests'!

!PlainConsumeTransformTest methodsFor: 'tests'!

testModelTransformSentToAxon
| model result axon |
result := nil.
model := #{ 'foo' -> #('bar' #(1 #(2 3)) 'baz'). 'moo' -> 'zoo' }.
axon := TestSpyAxon new.
model axxord: axon.
model axes: #(foo 2) transform: [:r | #new].
self assert: axon changedAspectLog equals: #((foo 2))
!

testNontrivialModelGetsAppropriateValueForTransform
| model result |
result := nil.
model := #{ 'foo' -> #('bar' #(1 #(2 5)) 'baz'). 'moo' -> 'zoo' }.
model axes: #(foo 2) transform: [:r | result := r].
self assert: #(1 #(2 5)) equals: result
!

testNontrivialModelReturnsAppropriateValue
| model result |
result := nil.
model := #{ 'foo' -> #('bar' #(1 #(2 3)) 'baz'). 'moo' -> 'zoo' }.
model axes: #(foo 2) consume: [:r | result := r].
self assert: #(1 #(2 3)) equals: result
!

testNontrivialModelTransformsAppropriateValue
| model result |
result := nil.
model := #{ 'foo' -> #('bar' #(1 #(2 3)) 'baz'). 'moo' -> 'zoo' }.
model axes: #(foo 2) transform: [:r | #new].
model axes: #(foo 2) consume: [:r | result := r].
self assert: #new equals: result.
model axes: #() consume: [:r | result := r].
self assert: #{ 'foo' -> #('bar' #new 'baz'). 'moo' -> 'zoo' } equals: result
!

testRootModelCannotTransform
| model result |
result := nil.
model := #(1 #(2 3)).
self should: [ model axes: #() transform: [:r | #{'foo'->#(4 5 6)}] ] raise: Error
!

testRootModelReturnsDirectRoot
| model result |
result := nil.
model := #(1 #(2 4)).
model axes: #() consume: [:r | r at: 2 put: nil].
model axes: #() consume: [:r | result := r].
self assert: #(1 nil) equals: result
!

testRootModelReturnsRoot
| model result |
result := nil.
model := #(1 #(2 3)).
model axes: #() consume: [:r | result := r].
self assert: #(1 #(2 3)) equals: result
!

testRootTransformBlockIsRun
| model result |
result := nil.
model := #(2 #(1 0)).
[model axes: #() transform: [:r | result := r. model]] on: Error do: [].
self assert: result equals: model
!

testRootTransformFailsOnActualChange
| model |
model := #(2 #(1 0)).
self should: [model axes: #() transform: [:r | #new]] raise: Error
! !

Axon subclass: #TestSpyAxon
	instanceVariableNames: 'changedAspectLog'
	package: 'Axxord-Tests'!
!TestSpyAxon commentStamp!
I am an axon that logs changed aspects. I am useful in tests.!

!TestSpyAxon methodsFor: 'accessing'!

changedAspectLog
	^ changedAspectLog
!

changedAspectLog: anObject
	changedAspectLog := anObject
! !

!TestSpyAxon methodsFor: 'action'!

changed: anAspect
	changedAspectLog add: anAspect
! !

!TestSpyAxon methodsFor: 'initialization'!

initialize
	super initialize.

	changedAspectLog := #()
! !

