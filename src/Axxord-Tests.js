define(["amber/boot", "amber_core/SUnit", "axxord/Axxord-Axon"], function($boot){"use strict";
if(!("nilAsValue" in $boot))$boot.nilAsValue=$boot.nilAsReceiver;
var $core=$boot.api,nil=$boot.nilAsValue,$nil=$boot.nilAsReceiver,$recv=$boot.asReceiver,$globals=$boot.globals;
$core.addPackage("Axxord-Tests");
($core.packageDescriptors||$core.packages)["Axxord-Tests"].innerEval = function (expr) { return eval(expr); };
($core.packageDescriptors||$core.packages)["Axxord-Tests"].transport = {"type":"amd","amdNamespace":"axxord"};

$core.addClass("AxolatorTest", $globals.TestCase, ["rootModel"], "Axxord-Tests");
$core.addMethod(
$core.method({
selector: "testNontrivialModelGetsAppropriateValueForModification",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var isolator,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
isolator=$recv($globals.Axolator)._on_($globals.HashedCollection._newFromPairs_(["foo",["bar", [(1), [(2), (5)]], "baz"],"moo","zoo"]));
$recv(isolator)._axes_transform_(["foo", (2)],(function(r){
result=r;
return result;

}));
$self._assert_equals_([(1), [(2), (5)]],result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testNontrivialModelGetsAppropriateValueForModification",{isolator:isolator,result:result},$globals.AxolatorTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testNontrivialModelGetsAppropriateValueForModification\x0a| isolator result |\x0aresult := nil.\x0aisolator := Axolator on: #{ 'foo' -> #('bar' #(1 #(2 5)) 'baz'). 'moo' -> 'zoo' }.\x0aisolator axes: #(foo 2) transform: [:r|result := r].\x0aself assert: #(1 #(2 5)) equals: result",
referencedClasses: ["Axolator"],
//>>excludeEnd("ide");
messageSends: ["on:", "axes:transform:", "assert:equals:"]
}),
$globals.AxolatorTest);

$core.addMethod(
$core.method({
selector: "testNontrivialModelModifiesAppropriateValue",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var isolator,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
isolator=$recv($globals.Axolator)._on_($globals.HashedCollection._newFromPairs_(["foo",["bar", [(1), [(2), (3)]], "baz"],"moo","zoo"]));
$recv(isolator)._axes_transform_(["foo", (2)],(function(r){
return "new";

}));
$recv(isolator)._axes_consume_(["foo", (2)],(function(r){
result=r;
return result;

}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.sendIdx["axes:consume:"]=1;
//>>excludeEnd("ctx");
$self._assert_equals_("new",result);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.sendIdx["assert:equals:"]=1;
//>>excludeEnd("ctx");
$recv(isolator)._axes_consume_([],(function(r){
result=r;
return result;

}));
$self._assert_equals_($globals.HashedCollection._newFromPairs_(["foo",["bar", "new", "baz"],"moo","zoo"]),result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testNontrivialModelModifiesAppropriateValue",{isolator:isolator,result:result},$globals.AxolatorTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testNontrivialModelModifiesAppropriateValue\x0a| isolator result |\x0aresult := nil.\x0aisolator := Axolator on: #{ 'foo' -> #('bar' #(1 #(2 3)) 'baz'). 'moo' -> 'zoo' }.\x0aisolator axes: #(foo 2) transform: [:r|#new].\x0aisolator axes: #(foo 2) consume: [:r|result := r].\x0aself assert: #new equals: result.\x0aisolator axes: #() consume: [:r|result := r].\x0aself assert: #{ 'foo' -> #('bar' #new 'baz'). 'moo' -> 'zoo' } equals: result",
referencedClasses: ["Axolator"],
//>>excludeEnd("ide");
messageSends: ["on:", "axes:transform:", "axes:consume:", "assert:equals:"]
}),
$globals.AxolatorTest);

$core.addMethod(
$core.method({
selector: "testNontrivialModelReturnsAppropriateValue",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var isolator,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
isolator=$recv($globals.Axolator)._on_($globals.HashedCollection._newFromPairs_(["foo",["bar", [(1), [(2), (3)]], "baz"],"moo","zoo"]));
$recv(isolator)._axes_consume_(["foo", (2)],(function(r){
result=r;
return result;

}));
$self._assert_equals_([(1), [(2), (3)]],result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testNontrivialModelReturnsAppropriateValue",{isolator:isolator,result:result},$globals.AxolatorTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testNontrivialModelReturnsAppropriateValue\x0a| isolator result |\x0aresult := nil.\x0aisolator := Axolator on: #{ 'foo' -> #('bar' #(1 #(2 3)) 'baz'). 'moo' -> 'zoo' }.\x0aisolator axes: #(foo 2) consume: [:r|result := r].\x0aself assert: #(1 #(2 3)) equals: result",
referencedClasses: ["Axolator"],
//>>excludeEnd("ide");
messageSends: ["on:", "axes:consume:", "assert:equals:"]
}),
$globals.AxolatorTest);

$core.addMethod(
$core.method({
selector: "testRootModelExaminesThenModifiesRoot",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var isolator,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
isolator=$recv($globals.Axolator)._on_([(1), [(2), (3)]]);
$recv(isolator)._axes_transform_([],(function(r){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(r)._second();
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({r:r},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv(isolator)._axes_consume_([],(function(r){
result=r;
return result;

}));
$self._assert_equals_([(2), (3)],result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testRootModelExaminesThenModifiesRoot",{isolator:isolator,result:result},$globals.AxolatorTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testRootModelExaminesThenModifiesRoot\x0a| isolator result |\x0aresult := nil.\x0aisolator := Axolator on: #(1 #(2 3)).\x0aisolator axes: #() transform: [:r|r second].\x0aisolator axes: #() consume: [:r|result := r].\x0aself assert: #(2 3) equals: result",
referencedClasses: ["Axolator"],
//>>excludeEnd("ide");
messageSends: ["on:", "axes:transform:", "second", "axes:consume:", "assert:equals:"]
}),
$globals.AxolatorTest);

$core.addMethod(
$core.method({
selector: "testRootModelGetsRootForModification",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var isolator,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
isolator=$recv($globals.Axolator)._on_([(2), [(1), (0)]]);
$recv(isolator)._axes_transform_([],(function(r){
result=r;
return result;

}));
$self._assert_equals_([(2), [(1), (0)]],result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testRootModelGetsRootForModification",{isolator:isolator,result:result},$globals.AxolatorTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testRootModelGetsRootForModification\x0a| isolator result |\x0aresult := nil.\x0aisolator := Axolator on: #(2 #(1 0)).\x0aisolator axes: #() transform: [:r|result := r].\x0aself assert: #(2 #(1 0)) equals: result",
referencedClasses: ["Axolator"],
//>>excludeEnd("ide");
messageSends: ["on:", "axes:transform:", "assert:equals:"]
}),
$globals.AxolatorTest);

$core.addMethod(
$core.method({
selector: "testRootModelModifiesAndDeeplyIsolatesInPlaceModifiedRoot",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var isolator,result,newValue;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
newValue=nil;
isolator=$recv($globals.Axolator)._on_([(1), [(2), (3)]]);
$recv(isolator)._axes_transform_([],(function(r){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
newValue=r;
newValue;
$recv(r)._at_put_((1),(4));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx2.sendIdx["at:put:"]=1;
//>>excludeEnd("ctx");
return r;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({r:r},$ctx1,1)});
//>>excludeEnd("ctx");
}));
$recv(newValue)._at_put_((2),"bar");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.sendIdx["at:put:"]=2;
//>>excludeEnd("ctx");
$recv(isolator)._axes_consume_([],(function(r){
result=r;
return result;

}));
$recv(newValue)._at_put_((2),"baz");
$self._assert_equals_([(4), [(2), (3)]],result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testRootModelModifiesAndDeeplyIsolatesInPlaceModifiedRoot",{isolator:isolator,result:result,newValue:newValue},$globals.AxolatorTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testRootModelModifiesAndDeeplyIsolatesInPlaceModifiedRoot\x0a| isolator result newValue |\x0aresult := nil. newValue := nil.\x0aisolator := Axolator on: #(1 #(2 3)).\x0aisolator axes: #() transform: [:r|newValue := r. r at: 1 put: 4. r].\x0anewValue at: 2 put: 'bar'.\x0aisolator axes: #() consume: [:r|result := r].\x0anewValue at: 2 put: 'baz'.\x0aself assert: #(4 #(2 3)) equals: result",
referencedClasses: ["Axolator"],
//>>excludeEnd("ide");
messageSends: ["on:", "axes:transform:", "at:put:", "axes:consume:", "assert:equals:"]
}),
$globals.AxolatorTest);

$core.addMethod(
$core.method({
selector: "testRootModelModifiesAndDeeplyIsolatesRoot",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var isolator,result,newValue;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
var $1;
result=nil;
isolator=$recv($globals.Axolator)._on_([(1), [(2), (3)]]);
newValue=$globals.HashedCollection._newFromPairs_(["foo",[(4), (5), (6)]]);
$recv(isolator)._axes_transform_([],(function(r){
return newValue;

}));
$1=$recv(newValue)._at_("foo");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.sendIdx["at:"]=1;
//>>excludeEnd("ctx");
$recv($1)._at_put_((1),"bar");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.sendIdx["at:put:"]=1;
//>>excludeEnd("ctx");
$recv(isolator)._axes_consume_([],(function(r){
result=r;
return result;

}));
$recv($recv(newValue)._at_("foo"))._at_put_((3),"baz");
$self._assert_equals_($globals.HashedCollection._newFromPairs_(["foo",[(4), (5), (6)]]),result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testRootModelModifiesAndDeeplyIsolatesRoot",{isolator:isolator,result:result,newValue:newValue},$globals.AxolatorTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testRootModelModifiesAndDeeplyIsolatesRoot\x0a| isolator result newValue |\x0aresult := nil.\x0aisolator := Axolator on: #(1 #(2 3)).\x0anewValue := #{'foo'->#(4 5 6)}.\x0aisolator axes: #() transform: [:r|newValue].\x0a(newValue at: 'foo') at: 1 put: 'bar'.\x0aisolator axes: #() consume: [:r|result := r].\x0a(newValue at: 'foo') at: 3 put: 'baz'.\x0aself assert: #{'foo'->#(4 5 6)} equals: result",
referencedClasses: ["Axolator"],
//>>excludeEnd("ide");
messageSends: ["on:", "axes:transform:", "at:put:", "at:", "axes:consume:", "assert:equals:"]
}),
$globals.AxolatorTest);

$core.addMethod(
$core.method({
selector: "testRootModelModifiesAndIsolatesRoot",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var isolator,result,newValue;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
isolator=$recv($globals.Axolator)._on_([(1), [(2), (3)]]);
newValue=$globals.HashedCollection._newFromPairs_(["foo",[(4), (5), (6)]]);
$recv(isolator)._axes_transform_([],(function(r){
return newValue;

}));
$recv(newValue)._at_put_("foo","bar");
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.sendIdx["at:put:"]=1;
//>>excludeEnd("ctx");
$recv(isolator)._axes_consume_([],(function(r){
result=r;
return result;

}));
$recv(newValue)._at_put_("foo","baz");
$self._assert_equals_($globals.HashedCollection._newFromPairs_(["foo",[(4), (5), (6)]]),result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testRootModelModifiesAndIsolatesRoot",{isolator:isolator,result:result,newValue:newValue},$globals.AxolatorTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testRootModelModifiesAndIsolatesRoot\x0a| isolator result newValue |\x0aresult := nil.\x0aisolator := Axolator on: #(1 #(2 3)).\x0anewValue := #{'foo'->#(4 5 6)}.\x0aisolator axes: #() transform: [:r|newValue].\x0anewValue at: 'foo' put: 'bar'.\x0aisolator axes: #() consume: [:r|result := r].\x0anewValue at: 'foo' put: 'baz'.\x0aself assert: #{'foo'->#(4 5 6)} equals: result",
referencedClasses: ["Axolator"],
//>>excludeEnd("ide");
messageSends: ["on:", "axes:transform:", "at:put:", "axes:consume:", "assert:equals:"]
}),
$globals.AxolatorTest);

$core.addMethod(
$core.method({
selector: "testRootModelModifiesRoot",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var isolator,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
isolator=$recv($globals.Axolator)._on_([(1), [(2), (3)]]);
$recv(isolator)._axes_transform_([],(function(r){
return $globals.HashedCollection._newFromPairs_(["foo",[(4), (5), (6)]]);

}));
$recv(isolator)._axes_consume_([],(function(r){
result=r;
return result;

}));
$self._assert_equals_($globals.HashedCollection._newFromPairs_(["foo",[(4), (5), (6)]]),result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testRootModelModifiesRoot",{isolator:isolator,result:result},$globals.AxolatorTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testRootModelModifiesRoot\x0a| isolator result |\x0aresult := nil.\x0aisolator := Axolator on: #(1 #(2 3)).\x0aisolator axes: #() transform: [:r|#{'foo'->#(4 5 6)}].\x0aisolator axes: #() consume: [:r|result := r].\x0aself assert: #{'foo'->#(4 5 6)} equals: result",
referencedClasses: ["Axolator"],
//>>excludeEnd("ide");
messageSends: ["on:", "axes:transform:", "axes:consume:", "assert:equals:"]
}),
$globals.AxolatorTest);

$core.addMethod(
$core.method({
selector: "testRootModelReturnsDeeplyIsolatedRoot",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var isolator,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
isolator=$recv($globals.Axolator)._on_([(1), [(2), (3)]]);
$recv(isolator)._axes_consume_([],(function(r){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv($recv(r)._at_((2)))._at_put_((1),(0));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({r:r},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.sendIdx["axes:consume:"]=1;
//>>excludeEnd("ctx");
$recv(isolator)._axes_consume_([],(function(r){
result=r;
return result;

}));
$self._assert_equals_([(1), [(2), (3)]],result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testRootModelReturnsDeeplyIsolatedRoot",{isolator:isolator,result:result},$globals.AxolatorTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testRootModelReturnsDeeplyIsolatedRoot\x0a| isolator result |\x0aresult := nil.\x0aisolator := Axolator on: #(1 #(2 3)).\x0aisolator axes: #() consume: [:r|(r at: 2) at: 1 put: 0].\x0aisolator axes: #() consume: [:r|result := r].\x0aself assert: #(1 #(2 3)) equals: result",
referencedClasses: ["Axolator"],
//>>excludeEnd("ide");
messageSends: ["on:", "axes:consume:", "at:put:", "at:", "assert:equals:"]
}),
$globals.AxolatorTest);

$core.addMethod(
$core.method({
selector: "testRootModelReturnsIsolatedRoot",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var isolator,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
isolator=$recv($globals.Axolator)._on_([(1), [(2), (4)]]);
$recv(isolator)._axes_consume_([],(function(r){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(r)._at_put_((2),nil);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({r:r},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.sendIdx["axes:consume:"]=1;
//>>excludeEnd("ctx");
$recv(isolator)._axes_consume_([],(function(r){
result=r;
return result;

}));
$self._assert_equals_([(1), [(2), (4)]],result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testRootModelReturnsIsolatedRoot",{isolator:isolator,result:result},$globals.AxolatorTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testRootModelReturnsIsolatedRoot\x0a| isolator result |\x0aresult := nil.\x0aisolator := Axolator on: #(1 #(2 4)).\x0aisolator axes: #() consume: [:r|r at: 2 put: nil].\x0aisolator axes: #() consume: [:r|result := r].\x0aself assert: #(1 #(2 4)) equals: result",
referencedClasses: ["Axolator"],
//>>excludeEnd("ide");
messageSends: ["on:", "axes:consume:", "at:put:", "assert:equals:"]
}),
$globals.AxolatorTest);

$core.addMethod(
$core.method({
selector: "testRootModelReturnsRoot",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var isolator,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
isolator=$recv($globals.Axolator)._on_([(1), [(2), (3)]]);
$recv(isolator)._axes_consume_([],(function(r){
result=r;
return result;

}));
$self._assert_equals_([(1), [(2), (3)]],result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testRootModelReturnsRoot",{isolator:isolator,result:result},$globals.AxolatorTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testRootModelReturnsRoot\x0a| isolator result |\x0aresult := nil.\x0aisolator := Axolator on: #(1 #(2 3)).\x0aisolator axes: #() consume: [:r|result := r].\x0aself assert: #(1 #(2 3)) equals: result",
referencedClasses: ["Axolator"],
//>>excludeEnd("ide");
messageSends: ["on:", "axes:consume:", "assert:equals:"]
}),
$globals.AxolatorTest);



$core.addClass("PlainConsumeTransformTest", $globals.TestCase, [], "Axxord-Tests");
$core.addMethod(
$core.method({
selector: "testModelTransformSentToAxon",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var model,result,axon;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
model=$globals.HashedCollection._newFromPairs_(["foo",["bar", [(1), [(2), (3)]], "baz"],"moo","zoo"]);
axon=$recv($globals.TestSpyAxon)._new();
$recv(model)._axxord_(axon);
$recv(model)._axes_transform_(["foo", (2)],(function(r){
return "new";

}));
$self._assert_equals_($recv(axon)._changedAspectLog(),[["foo", (2)]]);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testModelTransformSentToAxon",{model:model,result:result,axon:axon},$globals.PlainConsumeTransformTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testModelTransformSentToAxon\x0a| model result axon |\x0aresult := nil.\x0amodel := #{ 'foo' -> #('bar' #(1 #(2 3)) 'baz'). 'moo' -> 'zoo' }.\x0aaxon := TestSpyAxon new.\x0amodel axxord: axon.\x0amodel axes: #(foo 2) transform: [:r | #new].\x0aself assert: axon changedAspectLog equals: #((foo 2))",
referencedClasses: ["TestSpyAxon"],
//>>excludeEnd("ide");
messageSends: ["new", "axxord:", "axes:transform:", "assert:equals:", "changedAspectLog"]
}),
$globals.PlainConsumeTransformTest);

$core.addMethod(
$core.method({
selector: "testNontrivialModelGetsAppropriateValueForTransform",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var model,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
model=$globals.HashedCollection._newFromPairs_(["foo",["bar", [(1), [(2), (5)]], "baz"],"moo","zoo"]);
$recv(model)._axes_transform_(["foo", (2)],(function(r){
result=r;
return result;

}));
$self._assert_equals_([(1), [(2), (5)]],result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testNontrivialModelGetsAppropriateValueForTransform",{model:model,result:result},$globals.PlainConsumeTransformTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testNontrivialModelGetsAppropriateValueForTransform\x0a| model result |\x0aresult := nil.\x0amodel := #{ 'foo' -> #('bar' #(1 #(2 5)) 'baz'). 'moo' -> 'zoo' }.\x0amodel axes: #(foo 2) transform: [:r | result := r].\x0aself assert: #(1 #(2 5)) equals: result",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["axes:transform:", "assert:equals:"]
}),
$globals.PlainConsumeTransformTest);

$core.addMethod(
$core.method({
selector: "testNontrivialModelReturnsAppropriateValue",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var model,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
model=$globals.HashedCollection._newFromPairs_(["foo",["bar", [(1), [(2), (3)]], "baz"],"moo","zoo"]);
$recv(model)._axes_consume_(["foo", (2)],(function(r){
result=r;
return result;

}));
$self._assert_equals_([(1), [(2), (3)]],result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testNontrivialModelReturnsAppropriateValue",{model:model,result:result},$globals.PlainConsumeTransformTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testNontrivialModelReturnsAppropriateValue\x0a| model result |\x0aresult := nil.\x0amodel := #{ 'foo' -> #('bar' #(1 #(2 3)) 'baz'). 'moo' -> 'zoo' }.\x0amodel axes: #(foo 2) consume: [:r | result := r].\x0aself assert: #(1 #(2 3)) equals: result",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["axes:consume:", "assert:equals:"]
}),
$globals.PlainConsumeTransformTest);

$core.addMethod(
$core.method({
selector: "testNontrivialModelTransformsAppropriateValue",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var model,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
model=$globals.HashedCollection._newFromPairs_(["foo",["bar", [(1), [(2), (3)]], "baz"],"moo","zoo"]);
$recv(model)._axes_transform_(["foo", (2)],(function(r){
return "new";

}));
$recv(model)._axes_consume_(["foo", (2)],(function(r){
result=r;
return result;

}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.sendIdx["axes:consume:"]=1;
//>>excludeEnd("ctx");
$self._assert_equals_("new",result);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.sendIdx["assert:equals:"]=1;
//>>excludeEnd("ctx");
$recv(model)._axes_consume_([],(function(r){
result=r;
return result;

}));
$self._assert_equals_($globals.HashedCollection._newFromPairs_(["foo",["bar", "new", "baz"],"moo","zoo"]),result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testNontrivialModelTransformsAppropriateValue",{model:model,result:result},$globals.PlainConsumeTransformTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testNontrivialModelTransformsAppropriateValue\x0a| model result |\x0aresult := nil.\x0amodel := #{ 'foo' -> #('bar' #(1 #(2 3)) 'baz'). 'moo' -> 'zoo' }.\x0amodel axes: #(foo 2) transform: [:r | #new].\x0amodel axes: #(foo 2) consume: [:r | result := r].\x0aself assert: #new equals: result.\x0amodel axes: #() consume: [:r | result := r].\x0aself assert: #{ 'foo' -> #('bar' #new 'baz'). 'moo' -> 'zoo' } equals: result",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["axes:transform:", "axes:consume:", "assert:equals:"]
}),
$globals.PlainConsumeTransformTest);

$core.addMethod(
$core.method({
selector: "testRootModelCannotTransform",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var model,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
model=[(1), [(2), (3)]];
$self._should_raise_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(model)._axes_transform_([],(function(r){
return $globals.HashedCollection._newFromPairs_(["foo",[(4), (5), (6)]]);

}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}),$globals.Error);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testRootModelCannotTransform",{model:model,result:result},$globals.PlainConsumeTransformTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testRootModelCannotTransform\x0a| model result |\x0aresult := nil.\x0amodel := #(1 #(2 3)).\x0aself should: [ model axes: #() transform: [:r | #{'foo'->#(4 5 6)}] ] raise: Error",
referencedClasses: ["Error"],
//>>excludeEnd("ide");
messageSends: ["should:raise:", "axes:transform:"]
}),
$globals.PlainConsumeTransformTest);

$core.addMethod(
$core.method({
selector: "testRootModelReturnsDirectRoot",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var model,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
model=[(1), [(2), (4)]];
$recv(model)._axes_consume_([],(function(r){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(r)._at_put_((2),nil);
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({r:r},$ctx1,1)});
//>>excludeEnd("ctx");
}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.sendIdx["axes:consume:"]=1;
//>>excludeEnd("ctx");
$recv(model)._axes_consume_([],(function(r){
result=r;
return result;

}));
$self._assert_equals_([(1), nil],result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testRootModelReturnsDirectRoot",{model:model,result:result},$globals.PlainConsumeTransformTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testRootModelReturnsDirectRoot\x0a| model result |\x0aresult := nil.\x0amodel := #(1 #(2 4)).\x0amodel axes: #() consume: [:r | r at: 2 put: nil].\x0amodel axes: #() consume: [:r | result := r].\x0aself assert: #(1 nil) equals: result",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["axes:consume:", "at:put:", "assert:equals:"]
}),
$globals.PlainConsumeTransformTest);

$core.addMethod(
$core.method({
selector: "testRootModelReturnsRoot",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var model,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
model=[(1), [(2), (3)]];
$recv(model)._axes_consume_([],(function(r){
result=r;
return result;

}));
$self._assert_equals_([(1), [(2), (3)]],result);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testRootModelReturnsRoot",{model:model,result:result},$globals.PlainConsumeTransformTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testRootModelReturnsRoot\x0a| model result |\x0aresult := nil.\x0amodel := #(1 #(2 3)).\x0amodel axes: #() consume: [:r | result := r].\x0aself assert: #(1 #(2 3)) equals: result",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["axes:consume:", "assert:equals:"]
}),
$globals.PlainConsumeTransformTest);

$core.addMethod(
$core.method({
selector: "testRootTransformBlockIsRun",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var model,result;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
result=nil;
model=[(2), [(1), (0)]];
$recv((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(model)._axes_transform_([],(function(r){
result=r;
result;
return model;

}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}))._on_do_($globals.Error,(function(){

}));
$self._assert_equals_(result,model);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testRootTransformBlockIsRun",{model:model,result:result},$globals.PlainConsumeTransformTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testRootTransformBlockIsRun\x0a| model result |\x0aresult := nil.\x0amodel := #(2 #(1 0)).\x0a[model axes: #() transform: [:r | result := r. model]] on: Error do: [].\x0aself assert: result equals: model",
referencedClasses: ["Error"],
//>>excludeEnd("ide");
messageSends: ["on:do:", "axes:transform:", "assert:equals:"]
}),
$globals.PlainConsumeTransformTest);

$core.addMethod(
$core.method({
selector: "testRootTransformFailsOnActualChange",
protocol: "tests",
fn: function (){
var self=this,$self=this;
var model;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
model=[(2), [(1), (0)]];
$self._should_raise_((function(){
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx2) {
//>>excludeEnd("ctx");
return $recv(model)._axes_transform_([],(function(r){
return "new";

}));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx2) {$ctx2.fillBlock({},$ctx1,1)});
//>>excludeEnd("ctx");
}),$globals.Error);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"testRootTransformFailsOnActualChange",{model:model},$globals.PlainConsumeTransformTest)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "testRootTransformFailsOnActualChange\x0a| model |\x0amodel := #(2 #(1 0)).\x0aself should: [model axes: #() transform: [:r | #new]] raise: Error",
referencedClasses: ["Error"],
//>>excludeEnd("ide");
messageSends: ["should:raise:", "axes:transform:"]
}),
$globals.PlainConsumeTransformTest);



$core.addClass("TestSpyAxon", $globals.Axon, ["changedAspectLog"], "Axxord-Tests");
//>>excludeStart("ide", pragmas.excludeIdeData);
$globals.TestSpyAxon.comment="I am an axon that logs changed aspects. I am useful in tests.";
//>>excludeEnd("ide");
$core.addMethod(
$core.method({
selector: "changed:",
protocol: "action",
fn: function (anAspect){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
$recv($self["@changedAspectLog"])._add_(anAspect);
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"changed:",{anAspect:anAspect},$globals.TestSpyAxon)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anAspect"],
source: "changed: anAspect\x0a\x09changedAspectLog add: anAspect",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["add:"]
}),
$globals.TestSpyAxon);

$core.addMethod(
$core.method({
selector: "changedAspectLog",
protocol: "accessing",
fn: function (){
var self=this,$self=this;
return $self["@changedAspectLog"];

},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "changedAspectLog\x0a\x09^ changedAspectLog",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: []
}),
$globals.TestSpyAxon);

$core.addMethod(
$core.method({
selector: "changedAspectLog:",
protocol: "accessing",
fn: function (anObject){
var self=this,$self=this;
$self["@changedAspectLog"]=anObject;
return self;

},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: ["anObject"],
source: "changedAspectLog: anObject\x0a\x09changedAspectLog := anObject",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: []
}),
$globals.TestSpyAxon);

$core.addMethod(
$core.method({
selector: "initialize",
protocol: "initialization",
fn: function (){
var self=this,$self=this;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
return $core.withContext(function($ctx1) {
//>>excludeEnd("ctx");
(
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = true,
//>>excludeEnd("ctx");
($globals.TestSpyAxon.superclass||$boot.nilAsClass).fn.prototype._initialize.apply($self, []));
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
$ctx1.supercall = false;
//>>excludeEnd("ctx");;
$self["@changedAspectLog"]=[];
return self;
//>>excludeStart("ctx", pragmas.excludeDebugContexts);
}, function($ctx1) {$ctx1.fill(self,"initialize",{},$globals.TestSpyAxon)});
//>>excludeEnd("ctx");
},
//>>excludeStart("ide", pragmas.excludeIdeData);
args: [],
source: "initialize\x0a\x09super initialize.\x0a\x0a\x09changedAspectLog := #()",
referencedClasses: [],
//>>excludeEnd("ide");
messageSends: ["initialize"]
}),
$globals.TestSpyAxon);


});
